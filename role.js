const Sequelize = require('sequelize');
var bcrypt = require('bcrypt');

// create a sequelize instance with our local postgres database information.
var sequelize = new Sequelize('sql11217650', 'sql11217650', 'uXWFLLlUcH', {host:'sql11.freemysqlhosting.net', dialect:'mysql'});;

// setup User model and its fields.
var Role = sequelize.define('roles', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  role: {
    type: Sequelize.STRING,
    unique: true,
    allowNull: false
  }
});

// create all the defined tables in the specified database.
sequelize.sync().then(() => {

  console.log('users table has been successfully created, if one doesn\'t exist');
  sequelize.transaction((t) => {
    return Role.create({
      role: 'Administrator'
    }, {transaction: t})
  }).catch((err) => {})
  sequelize.transaction((t) => {
    return Role.create({
      role: 'Nastavnik'
    }, {transaction: t})
  }).catch((err) => {})
  sequelize.transaction((t) => {
    return Role.create({
      role: 'Student'
    }, {transaction: t})
  }).catch((err) => {})


}).catch(error => console.log('This error occured', error));

// export User model for use in other files.
module.exports = function(sequelize,DataTypes){
    return Role;
}

const Sequelize = require('sequelize');
var bcrypt = require('bcrypt');

// create a sequelize instance with our local postgres database information.
var sequelize = new Sequelize('sql11217650', 'sql11217650', 'uXWFLLlUcH', {host:'sql11.freemysqlhosting.net', dialect:'mysql'});

// setup User model and its fields.
var Korisnik = sequelize.define('korisnik', {
    id:{
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    username: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false
    },
    rola: {
        type: Sequelize.STRING,
        allowNull: false
    },
    verified: {
        type: Sequelize.INTEGER,
        allowNull: true
    }
}, {
    hooks: {
      beforeCreate: (user) => {
        const salt = bcrypt.genSaltSync();
        user.password = bcrypt.hashSync(user.password, salt);
      }
    }
});

Korisnik.prototype.validPassword = function(sifra) {
      return bcrypt.compareSync(sifra, this.password);
  }

// create all the defined tables in the specified database.
sequelize.sync()
    .then(() =>{ console.log('users table has been successfully created, if one doesn\'t exist')
    sequelize.transaction((t) => {
      return Korisnik.create({
        username: 'Muharem',
        password: 'Muharem',
        rola: 1,
      }, {transaction: t})
    }).catch((err) => {})
  }).catch(error => console.log('This error occured', error));

// export User model for use in other files.
module.exports = function(sequelize,DataTypes){
    return Korisnik;
}

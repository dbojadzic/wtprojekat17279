const Sequelize = require('sequelize');
const korisnik = require('./korisnik');

// create a sequelize instance with our local postgres database information.
var sequelize = new Sequelize('sql11217650', 'sql11217650', 'uXWFLLlUcH', {host:'sql11.freemysqlhosting.net', dialect:'mysql'});

// setup User model and its fields.
var licniPodatak = sequelize.define('licniPodaci', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    ImeiPrezime: {
        type: Sequelize.STRING,
        allowNull: false
    },
    Indexi: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: true
    },
    Grupa: {
        type: Sequelize.STRING,
        allowNull: true
    },
    AkGodina: {
        type: Sequelize.STRING,
        allowNull: true
    },
    BitbucketURL: {
        type: Sequelize.STRING,
        allowNull: true
    },
    BitbucketSSH: {
        type: Sequelize.STRING,
        allowNull: true
    },
    NazivRepo: {
        type: Sequelize.STRING,
        allowNull: true
    },
    Email: {
        type: Sequelize.STRING,
        allowNull: true
    },
    maxbrojgrupa: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    Regex: {
        type: Sequelize.STRING,
        allowNull: true
    },
    Semestar:{
      type: Sequelize.STRING,
      allowNull: true
    }

});

// create all the defined tables in the specified database.
sequelize.sync()
    .then(() => console.log('users table has been successfully created, if one doesn\'t exist'))
    .catch(error => console.log('This error occured', error));

// export User model for use in other files.
module.exports = function(sequelize,DataTypes){
    return licniPodatak;
}

var Validacija=(function(){
	var maxGrupa=7;
	var trenutniSemestar=0;//0 za zimski, 1 za ljetni semestar

	var validirajFakultetski=function(email){
		var regex = /\w+@etf\.unsa\.ba/;
		if(regex.test(email)){
			Poruke.ocistiGresku(0);
			return true;
		}
		Poruke.dodajPoruku(0);
		return false;
	}

	var validirajIndex=function(index){
		var regex = /1\d{4}/;
		if(regex.test(index)){
			Poruke.ocistiGresku(1);
			return true;
		}
		Poruke.dodajPoruku(1);
		return false;
	}

	var validirajGrupu=function(broj){
		if(broj >= 1 && broj <= maxGrupa){
			Poruke.ocistiGresku(2);
			return true;
		}
		Poruke.dodajPoruku(2);
		return false;
	}

	var validirajAkGod = function(god){
		var regex = /20\d{2}\/20\d{2}/;
		if (!regex.test(god)) {
			Poruke.dodajPoruku(3);
			return false;
		}
		else if(parseInt(god[2]+god[3]) + 1 != parseInt(god[7]+god[8])){
			Poruke.dodajPoruku(3);
			return false;
		}
		else{
			var godina = (new Date()).getFullYear();
			godina = godina - 2000;
			if(trenutniSemestar == 0){
				if(parseInt(god[2] + god[3]) == godina){
					Poruke.ocistiGresku(3);
					return true;
				}
				else{
					Poruke.dodajPoruku(3);
					return false;
				}
			}
			else{
				if(parseInt(god[7] + god[8]) == godina){
					Poruke.ocistiGresku(3);
					return true;
				}
				else{
					Poruke.dodajPoruku(3);
					return false;
				}
			}
		}
		Poruke.dodajPoruku(3);
		return false;
	}

	var validirajPassword = function(password){
		var regexduzina = /\w{7,20}/;
		var regexveliko = /.*[A-Z].*/;
		var regexmalo = /.*[a-z].*/;
		var regexbroj = /.*[0-9].*/;
		if(regexduzina.test(password) && regexveliko.test(password) && regexmalo.test(password) && regexbroj.test(password)){
			Poruke.ocistiGresku(4);
			return true;
		}
		Poruke.dodajPoruku(4);
		return false;
	}

	var validirajPotvrdu = function(pass1, pass2){
		if(pass1 == pass2 && validirajPassword(pass1)){
			Poruke.ocistiGresku(5);
			return true;
		}
		Poruke.dodajPoruku(5);
		return false;
	}

	var validirajBitbucketURL = function(url){
		var regex = /https:\/\/\w+@bitbucket\.org\/\w+\/\w+\.git/
		if(getRegex() != null)
		var regex2 = getRegex();
		else
		var regex2 = /.*wt[p|P]rojekat1\d{4}.*/
		if(regex.test(url) && regex2.test(url)){
			Poruke.ocistiGresku(6);
			return true;
		}
		Poruke.dodajPoruku(6);
		return false;
	}

	var validirajBitbucketSSH = function(ssh){
		var regex = /git@bitbucket\.org:\w+\/\w+\.git/;
		if(getRegex() != null)
		var regex2 = getRegex();
		else
		var regex2 = /.*wt[p|P]rojekat1\d{4}.*/
		if(regex.test(ssh) && regex2.test(ssh)){
			Poruke.ocistiGresku(7);
			return true;
		}
		Poruke.dodajPoruku(7);
		return false;
	}

	var validirajNazivRepozitorija = function(reg, naziv){
		if(reg == null){
			var regex2 = /wt[p|P]rojekat1\d{4}/
			if(regex2.test(naziv)){
				Poruke.ocistiGresku(8);
				return true;
			}
			Poruke.dodajPoruku(8);
			return false;
		}
		else{
			var regex = new RegExp("/" + reg + "/", "g");
			if(regex.test(naziv)){
				Poruke.ocistiGresku(8);
				return true;
			}
			Poruke.dodajPoruku(8);
			return false;
		}
	}

	var validirajImeiPrezime = function(ime){
		ime += ' ';
		var regex = /([A-Z|Č|Ć|Š|Ž|Đ][A-Z|a-z|č|ć|š|ž|đ|Č|Ć|Š|Ž|Đ]{2,11}[-|'|\ ])+/;
		//Objasnjenje regexa: Rijec pocinje velikim slovom, i onda ide 2-11 znakova koji
		//mogu biti velika slova ali i ne moraju, nakon toga ide separator - ' ili ' '
		//Separatori - ' i ' ' dijele riječi u mom slučaju
		//Na kraju sam dodao + sto znaci da se ova struktura moze ponavljati
		//Neće proći ako riječ na kraju ima razmak ili neki od znakova - ili '
		//To sam osigurao dodavanjem ' ' na kraju, zbog čega regex prolazi i rijec ostaje validna
		if(regex.test(ime)){
			Poruke.ocistiGresku(9);
			return true;
		}
		Poruke.dodajPoruku(9);
		return false;
	}

	var postaviMaxGrupa = function(max){
		maxGrupa = parseInt(max);
	}

	var postaviTrenSemestar = function(semestar){
		trenutniSemestar = parseInt(semestar);
	}

	return {
		validirajFakultetski : validirajFakultetski,
		validirajIndex : validirajIndex,
		validirajGrupu : validirajGrupu,
		validirajAkGod : validirajAkGod,
		validirajPassword : validirajPassword,
		validirajPotvrdu : validirajPotvrdu,
		validirajBitbucketURL : validirajBitbucketURL,
		validirajBitbucketSSH : validirajBitbucketSSH,
		validirajNazivRepozitorija : validirajNazivRepozitorija,
		validirajImeiPrezime : validirajImeiPrezime,
		postaviMaxGrupa : postaviMaxGrupa,
		postaviTrenSemestar : postaviTrenSemestar
	}
}());

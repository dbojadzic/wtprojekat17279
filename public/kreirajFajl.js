var kreirajFajl=(function(){
    return {
        kreirajKomentar: function(spirala, index, sadrzaj, fnCallback){
          /*
          console.log(spirala);
          console.log(index);
          console.log(sadrzaj);
          console.log(typeof sadrzaj);
          console.log(typeof spirala);
          console.log(typeof index);
          */
          if (typeof spirala == "string" && spirala.length >= 1 && typeof index == "string" && index.length >= 1 &&  typeof sadrzaj == "object" /* ne radi iz nekog razloga && sadrzaj[0].hasOwnProperty('sifra_studenta') && sadrzaj[0].hasOwnProperty('tekst') && sadrzaj[0].hasOwnProperty('ocjena')*/){
            var ajax = new XMLHttpRequest();
            ajax.onreadystatechange = function(){
                if (ajax.readyState == 4 && ajax.status == 200)
                    fnCallback (null, ajax.responseText);
                if (ajax.readyState == 4 && ajax.status != 200)
                    fnCallback (ajax.status, ajax.responseText);
            }
            var jsonobj = {'spirala':spirala,'index':index, 'sadrzaj':sadrzaj};
            ajax.open("POST", "komentar", true);
            ajax.setRequestHeader("Content-Type", "application/json");
            ajax.send(JSON.stringify (jsonobj));
          }
          else {
            fnCallback(-1, "Neispravni parametri");
          }
        },

        kreirajListu: function(godina, nizRepozitorija, fnCallback){
          if (typeof godina == "string" && godina.length >= 1 && nizRepozitorija instanceof Array && nizRepozitorija.length >= 1){
            var ajax = new XMLHttpRequest();
            ajax.onreadystatechange = function(){
                if (ajax.readyState == 4 && ajax.status == 200)
                    fnCallback (null, ajax.responseText);
                if (ajax.readyState == 4 && ajax.status != 200)
                    fnCallback(ajax.status, ajax.responseText);
            }
            var jsonobj = {'godina':godina, 'nizRepozitorija':nizRepozitorija};
            ajax.open("POST", "lista", true);
            ajax.setRequestHeader("Content-Type", "application/json");
            ajax.send(JSON.stringify(jsonobj));
          }
          else {
            fnCallback(-1, "Neispravni parametri");
          }
        },

        kreirajIzvjestaj: function(spirala, index, fnCallback){
          if (typeof spirala == "string" && spirala.length >= 1 && typeof index == "string" && index.length >= 1){
            var ajax = new XMLHttpRequest();
            ajax.onreadystatechange = function(){
                if (ajax.readyState == 4 && ajax.status == 200)
                    fnCallback (null, ajax.responseText);
                if (ajax.readyState == 4 && ajax.status != 200)
                    fnCallback (ajax.status, ajax.responseText);
            }
            var jsonobj = {'spirala':spirala,'index':index};
            ajax.open("POST", "izvjestaj", true);
            ajax.setRequestHeader("Content-Type", "application/json");
            ajax.send(JSON.stringify(jsonobj));
          }
          else {
            fnCallback(-1, "Neispravni parametri");
          }
        },

        kreirajBodove: function(spirala, index, fnCallback){
          if (typeof spirala == "string" && spirala.length >= 1 && typeof index == "string" && index.length >= 1){
            var ajax = new XMLHttpRequest();
            ajax.onreadystatechange = function(){
                if (ajax.readyState == 4 && ajax.status == 200)
                    fnCallback(null, ajax.responseText);
                if (ajax.readyState == 4 && ajax.status != 200)
                    fnCallback(ajax.status, ajax.responseText);
            }
            var jsonobj = {'spirala':spirala,'index':index};
            ajax.open("POST", "bodovi", true);
            ajax.setRequestHeader("Content-Type", "application/json");
            ajax.send(JSON.stringify(jsonobj));
          }
          else {
            fnCallback(-1, "Neispravni parametri");
          }
        }
    }
})();

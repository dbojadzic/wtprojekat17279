  var Poruke=(function() {
    var idDivaPoruka = null;
    var mogucePoruke=[
      "Email koji ste napisali nije validan fakultetski email",
      "Indeks kojeg ste napisali nije validan",
      "Nastavna grupa koju ste napisali nije validna",
      "Akademska godina koju ste napisali nije validna",
      "Password koji ste napisali nije validan",
      "Passwordi koje ste unijeli se ne poklapaju",
      "Bitbucket URL koji ste unijeli nije validan URL",
      "Bitbucket SSH koji ste unijeli nije validan SSH",
      "Naziv repozitorija koji ste unijeli nije validan",
      "Ime i prezime koje ste unijeli nije validno"];

      var porukeZaIspis=[];

      for (i=0; i<10; i++)
       porukeZaIspis.push("");

      var ispisiGreske=function() {
        if (idDivaPoruka==null)
          idDivaPoruka="Poruke";

        document.getElementById(idDivaPoruka).innerHTML="";
        for (i=0; i<10; i++)
          if(porukeZaIspis[i]!="")
            document.getElementById(idDivaPoruka).innerHTML += porukeZaIspis[i] + '.<br>';
      }

      var postaviIdDiva=function(id) {
        idDivaPoruka=id;
      }

      var dodajPoruku=function(brojPoruke) {
        if (brojPoruke<0 || brojPoruke>9)
          return;
        porukeZaIspis[brojPoruke] = mogucePoruke[brojPoruke];
        ispisiGreske();
      }

      var ocistiGresku=function(brojPoruke) {
        if (brojPoruke<0 || brojPoruke>9)
          return;
        porukeZaIspis[brojPoruke] = "";
        ispisiGreske();
      }

      return {
        ispisiGreske : ispisiGreske,
        postaviIdDiva : postaviIdDiva,
        dodajPoruku : dodajPoruku,
        ocistiGresku : ocistiGresku
      }
    }());

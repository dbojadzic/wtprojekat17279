function Unesi(){
  var text = document.getElementById("tekst").value;
  text = text.split(/\n/);
  var ssh = [];
  for(var i = 0; i<text.length; i++){
    var red = text[i];
    red = red.split(",");
    if(red.length != 6){
      document.getElementById("poruka").innerHTML = "Red " + i+1 + " ne zadovoljava uslove! Neispravan broj indexa!";
      return;
    }
    for(var j = 0; j<6; j++){
      var regex = /^1\d{4}$/;
      if(!regex.test(red[j])){
        document.getElementById("poruka").innerHTML = "Red " + i+1 + " ne zadovoljava uslove! Indexi nisu validni!";
        return;
      }
      if(red.includes(red[j],j+1)){
        document.getElementById("poruka").innerHTML = "Red " + i+1 + " ne zadovoljava uslove! Indexi se ponavljaju!";
        return;
      }
    }
    ssh.push(red);
  }
  var ajax = new XMLHttpRequest();
  ajax.onreadystatechange = function() {
    if (ajax.readyState == 4 && ajax.status != 200)
      return;
    if (ajax.readyState == 4 && ajax.status == 200)
      document.getElementById("poruka").innerHTML = ajax.responseText;
  }
  var spirala = document.getElementById("spirala").value;
  var write = {"data":ssh, "spirala":spirala};
  var write2 = JSON.stringify(write);
  ajax.open("POST", "spisak", true);
  ajax.setRequestHeader("Content-Type", "application/json");
  ajax.send(write2);
}

var gnaziv;
var gbranch;
var ggodina;
var gtoken;
var grepozitorij = [];
var grepozit;


var Generisi = function(key, secret, naziv, branch, godina){
  gnaziv = naziv;
  gbranch = branch;
  ggodina = godina;
  bitbucketApi.dohvatiAccessToken(key, secret, getToken);
  kreirajFajl.kreirajListu(godina, grepozitorij, Ispis4);
}

var getToken = function(status, response){
  if(status != null)
    document.getElementById("poruka").innerHTML = response;
  else{
    gtoken = response;
    bitbucketApi.dohvatiRepozitorije(gtoken, ggodina, gnaziv, gbranch, getBranches);
  }
}

var getBranches = function(status, response){
  if(status != null)
    document.getElementById("poruka").innerHTML = response;
  else{
    var nizRepozitorija = response;
    console.log(response);
    for(var i = 0; i<nizRepozitorija.length; i++){
      grepozit = nizRepozitorija[i];
      bitbucketApi.dohvatiBranch(gtoken, grepozit, naziv, PushRepo);
    }
  }
}

var PushRepo = function(status, response){
  if(status != null && response == true)
    grepozitorij.push(grepozit);
}

var Ispis4 = function(status, responseText){
  if(responseText != "Neispravni parametri"){
    responseText = JSON.parse(responseText);
    document.getElementById("poruka").innerHTML = responseText.message;
  }
  else
    document.getElementById("poruka").innerHTML = responseText;
}

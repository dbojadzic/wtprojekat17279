var bitbucketApi = (function(){
  return {
    dohvatiAccessToken: function(key, secret, fnCallback){
      if (key == null || secret == null){
        fnCallback(-1, "Key ili secret nisu pravilno proslijeđeni!");
      }
      else {
        //kopirano iz tutorijala
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function(){
          if (ajax.readyState == 4 && ajax.status == 200)
            fnCallback(null, JSON.parse(ajax.responseText).access_token);
          if (ajax.readyState == 4 && ajax.status != 200)
            fnCallback(ajax.status, ajax.responseText);
        }
        ajax.open("POST", "https://bitbucket.org/site/oauth2/access_token", true);
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.setRequestHeader("Authorization", 'Basic ' + btoa(key+':'+secret));
        ajax.send("grant_type="+encodeURIComponent("client_credentials"));
      }
    },

    dohvatiRepozitorije: function(token, godina, naziv, branch, fnCallback){
      var ajax = new XMLHttpRequest();
      ajax.onreadystatechange = function(){
        if (ajax.readyState == 4 && ajax.status == 200) {
          ssh = [];
          var podaci = JSON.parse(ajax.responseText).values;

          for(var i = 0; i<podaci.length; i++){
            console.log(podaci[i].name);
            console.log(podaci[i].name.includes(naziv));
            console.log(podaci[i].created_on);
            console.log(podaci[i].created_on.substring(0, 3));

            if(podaci[i].name.includes(naziv) && (podaci[i].created_on.substring(0, 4) == godina || (podaci[i].created_on.substring(0, 4) + 1)  == godina) /*&& podatak.mainbranch.name.contains(branch)*/)
              ssh.push(podaci[i].links.clone[1].href);
          }
          fnCallback(null, ssh);
        }
        if (ajax.readyState == 4 && ajax.status != 200)
        fnCallback(ajax.status, ajax.responseText);
      }
      //kopirano iz tutorijala
      ajax.open ("GET", "https://api.bitbucket.org/2.0/repositories?role=member" + "&pagelen=150");
      //ajax.open("GET", "https://api.bitbucket.org/2.0/repositories?role=member");
      ajax.setRequestHeader("Authorization", 'Bearer ' + token);
      ajax.send();
    },

    dohvatiBranch: function(token, url, naziv, fnCallback){
      var ajax = new XMLHttpRequest();
      ajax.onreadystatechange = function(){
        if (ajax.readyState == 4 && ajax.status == 200) {
          var podaci = JSON.parse(ajax.responseText).values;
          var nadjen = false;
          for (var podatak in podaci)
            if (podatak.name == naziv) {
              nadjen = true;
              break;
            }
          fnCallback (null, nadjen);
        }
        if (ajax.readyState == 4 && ajax.status != 200)
          fnCallback(ajax.status, ajax.responseText);
      }
      ajax.open("GET", encodeURI(url));
      ajax.setRequestHeader("Authorization", 'Bearer ' + token);
      ajax.send();
    }
  }
})();

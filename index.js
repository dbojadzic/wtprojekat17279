//Administrator Username: Muharem Password: Muharem
const express = require('express');
const bodyParser = require ('body-parser');
const Sequelize = require('sequelize');
var cookieParser = require('cookie-parser');
var session = require('express-session');
const fs = require('fs');
var korisnik = require('./korisnik.js');
var Podaci = require('./licniPodaci.js');
var Role = require('./role.js');
const sequelize = require('./baza.js')
var path = require ('path');
var pathName = path.join (__dirname, '/index.html');
const Korisnik = sequelize.import(__dirname+"/korisnik.js");
const Rola = sequelize.import(__dirname+"/role.js");
const licniPodaci = sequelize.import(__dirname+"/licniPodaci.js")
Korisnik.sync();
Rola.sync();
licniPodaci.sync();

var app = express();
app.use(bodyParser.json());
app.use(express.static("public"));

// initialize cookie-parser to allow us access the cookies stored in the browser.
app.use(cookieParser());

// initialize express-session to allow us track the logged-in user across sessions.
app.use(session({
  key: 'user_sid',
  secret: 'smth',
  resave: false,
  saveUninitialized: false,
  cookie: {
    expires: 600000
  }
}));

// This middleware will check if user's cookie is still saved in browser and user is not set, then automatically log the user out.
// This usually happens when you stop your express server after login, your cookie still remains saved in the browser.
app.use((req, res, next) => {
  if (req.cookies.user_sid && !req.session.user) {
    res.clearCookie('user_sid');
  }
  next();
});

let sessionChecker = (req, res, next) => {
  if (req.session.role && req.cookies.role_id) {
    res.redirect('/dashboard');
  } else {
    next();
  }
};

app.get('/', sessionChecker, function(req,res){
  res.sendFile(__dirname + '/index.html');
});

app.get('/login.html',function(req,res){
  res.sendFile(__dirname + '/login.html');
});

app.get('/listakorisnika.html',function(req,res){
  if (req.session.korisnik && req.cookies.user_sid && req.session.rola == 1) {

    res.sendFile(__dirname + '/listakorisnika.html');
} else {
      res.redirect('/login');
  }
});
/*
app.post('/login',function(req,res){
  var username = req.body.username,
  password = req.body.password;

  korisnik.findOne({ where: { username: username } }).then(function (korisnik) {
    if (!korisnik) {
      res.redirect('/login');
    } else if (!korisnik.validPassword(password)) {
      res.redirect('/login');
    } else {
      req.session.korisnik = user.dataValues;
      res.redirect('/dashboard');
    }
  });
});
*/
app.get('/statistika.html',function(req,res){
  if (req.session.korisnik && req.cookies.user_sid && req.session.rola == 2) {
  res.sendFile(__dirname + '/statistika.html');
} else {
      res.redirect('/login');
  }
});

app.get('/unoskomentara.html',function(req,res){
  if (req.session.korisnik && req.cookies.user_sid && req.session.rola == 3) {
  res.sendFile(__dirname + '/unoskomentara.html');
} else {
      res.redirect('/login');
  }
});

app.get('/unosSpiska.html',function(req,res){
  if (req.session.korisnik && req.cookies.user_sid && req.session.rola == 2) {
  res.sendFile(__dirname + '/unosSpiska.html');
} else {
      res.redirect('/login');
  }
});

app.get('/nastavnik.html',function(req,res){
  if (req.session.korisnik && req.cookies.user_sid && req.session.rola == 2) {
  res.sendFile(__dirname + '/nastavnik.html');
} else {
      res.redirect('/login');
  }
});

app.get('/bitbucketPozivi.html',function(req,res){
  if (req.session.korisnik && req.cookies.user_sid && req.session.rola == 2) {
  res.sendFile(__dirname + '/bitbucketPozivi.html');
} else {
      res.redirect('/login');
  }
});
/***************************/
app.post('/spisak',function(req,res){
  let tijeloZahtjeva = req.body;
  var indexi = tijeloZahtjeva.data;
  var spirala = tijeloZahtjeva.spirala;
  indexi = JSON.stringify(indexi);
  fs.appendFile ('spisakS' + spirala + '.json', indexi, function (err) {
    if (err) throw err;
    res.send("Datoteka kreirana");
  });
});

app.post('/komentar',function(req,res){
  let tijeloZahtjeva = req.body;
  if(tijeloZahtjeva.hasOwnProperty('spirala') && tijeloZahtjeva.hasOwnProperty('index') && tijeloZahtjeva.hasOwnProperty('sadrzaj') && tijeloZahtjeva.sadrzaj[0].hasOwnProperty('sifra_studenta') && tijeloZahtjeva.sadrzaj[0].hasOwnProperty('tekst') && tijeloZahtjeva.sadrzaj[0].hasOwnProperty('ocjena')){
    var spirala = tijeloZahtjeva.spirala;
    var index = tijeloZahtjeva.index;
    var podaci = JSON.stringify(tijeloZahtjeva.sadrzaj);
    fs.appendFile('markS' + spirala + index + '.json', podaci, function(err){
      if (err) throw err;
      res.json({"message":"Uspješno kreirana datoteka!", "data":podaci});
    });
  }
  else
  res.json({"message":"Podaci nisu u trazenom formatu!", "data":null});
});

app.post('/lista',function(req,res){
  let tijeloZahtjeva = req.body;
  if(tijeloZahtjeva.hasOwnProperty('godina') && tijeloZahtjeva.hasOwnProperty('nizRepozitorija')){
    var godina = tijeloZahtjeva.godina;
    var nizRepozitorija = tijeloZahtjeva.nizRepozitorija;
    var podaci = [];
    for(var i = 0; i<nizRepozitorija.length; i++)
    if(nizRepozitorija[i].includes(godina))
    podaci.push(nizRepozitorija[i]);
    fs.appendFile('spisak' + godina + '.txt', podaci, function(err){
      if(err) throw err;
      res.json({"message":"Lista uspješno kreirana!", "data":podaci.length});
    });
  }
  else
  res.json({"message":"Podaci nisu u trazenom formatu!", "data":null});
});

app.post('/izvjestaj',function(req,res){
  let tijeloZahtjeva = req.body;
  var spirala = tijeloZahtjeva.spirala;
  var index = tijeloZahtjeva.index;
  var komentari = [];
  fs.readFile(__dirname + '/spisakS' + spirala + '.json', 'utf-8', function(err, data){
    if(err) throw err;
    data = JSON.parse(data);
    var indexi = [];
    var pozicije = [];
    for(var i = 0; i<data.length; i++){
      var red = data[i];
      /*
      console.log(data);
      console.log(data[i]);
      console.log(red);
      console.log(red[i]);
      */
      if(red.indexOf(index) >= 1){
        indexi.push(red[0]);
        pozicije.push(red.indexOf(index));
      }
    }
    var k = 0;
    for(var i = 0; i<indexi.length; i++){
      fs.readFile(__dirname + '/markS' + spirala + indexi[i] + '.json', 'utf-8', function(err, data2){
        if(err) throw err;
        var mark = JSON.parse(data2);
        var pozicija = pozicije[k];
        k++;
        komentari.push(mark[pozicija-1].tekst);
        komentari.push("\n##########\n");
        if(k == indexi.length)
        fs.appendFile('izvjestajS' + spirala + index + '.txt', komentari, function(err){
          if(err) throw err;
          res.json({"message":"Datoteka uspješno kreirana!", "data":null});
        });

      });
    }
  });
});

app.post('/bodovi',function(req,res){
  let tijeloZahtjeva = req.body;
  var spirala = tijeloZahtjeva.spirala;
  var index = tijeloZahtjeva.index;
  var brojBodova = 0;
  var brojStudenata = 0;
  fs.readFile(__dirname + '/spisakS' + spirala + '.json', 'utf-8', function(err, data){
    if(err) throw err;
    data = JSON.parse(data);
    var indexi = [];
    var pozicije = [];
    for(var i = 0; i<data.length; i++){
      var red = data[i];
      /*
      console.log(data);
      console.log(data[i]);
      console.log(red);
      console.log(red[i]);
      */
      if(red.indexOf(index) >= 1){
        indexi.push(red[0]);
        pozicije.push(red.indexOf(index));
      }
    }
    var k = 0;
    for(var i = 0; i<indexi.length; i++){
      fs.readFile(__dirname + '/markS' + spirala + indexi[i] + '.json', 'utf-8', function(err, data2){
        if(err) throw err;
        var mark = JSON.parse(data2);
        var pozicija = pozicije[k];
        k++;
        brojBodova += mark[pozicija-1].ocjena;
        brojStudenata++;
        if(k == indexi.length){
          rezultat = Math.floor(brojBodova/brojStudenata)+1;
          res.json({"poruka":"Student " + index + " je ostvario u prosjeku " + rezultat + " mjesto."});
        }
      });
    }
  });
});

app.post('/LoginL', function (req, res) {
  let tijeloZahtjeva = req.body;
  console.log(tijeloZahtjeva);
  Korisnik.findOne({where:{'username':tijeloZahtjeva.username}}).then (function (r) {
      if(!r)
        res.redirect('/');
      else if (!r.validPassword(tijeloZahtjeva.password))
        res.redirect('/')
      else if(r.verified == false)
        res.send("Nije verifikovano");
      else {
        req.session.korisnik = r.dataValues;
        req.session.rola = r.rola;
        Rola.findOne({where:{'id':r.rola}}).then(function (r2){
          if(r2 && r2.id == 1)
            res.sendFile(__dirname + '/indexA.html');
          else if(r2 && r2.id == 2)
            res.sendFile(__dirname + '/indexB.html');
          else
            res.sendFile(__dirname + '/indexC.html');
        })
      }
  });
});

app.post('/LoginR', function (req, res) {
  let tijeloZahtjeva = req.body;
  var helpid=0;
  if(tijeloZahtjeva.osoba == 1){
  Korisnik.create({'username':tijeloZahtjeva.susername, 'password':tijeloZahtjeva.spassword, 'rola':3}).then(function(r){
      console.log(r);
      helpid = r.id;
      licniPodaci.create({'ImeiPrezime':tijeloZahtjeva.simeiprezime, 'Indexi':tijeloZahtjeva.sindex, 'Grupa':tijeloZahtjeva.sgrupa, 'AkGodina':tijeloZahtjeva.sgodina, 'BitbucketURL':tijeloZahtjeva.sbitbucketurl, 'BitbucketSSH':tijeloZahtjeva.bitbucketSSH, 'NazivRepo':tijeloZahtjeva.srepozitorij}).then(function(r2){
        console.log(r2);
      })
    });
  }
  else{
    Korisnik.create({'username':tijeloZahtjeva.pusername, 'password':tijeloZahtjeva.ppassword, 'rola':2, 'verified':false}).then(function(r){
      console.log(r);
      helpid = r.id;
      licniPodaci.create({'ImeiPrezime':tijeloZahtjeva.pimeiprezime, 'Email':tijeloZahtjeva.pemail, 'maxbrojgrupa':tijeloZahtjeva.pmaxgrupe, 'Regex':tijeloZahtjeva.pregex, 'Semestar':tijeloZahtjeva.psemestar, 'AkGodina':tijeloZahtjeva.pgodina}).then(function(r2){
        console.log(r2);
      })
    });
  }
});

app.post('/Pretraga', function(req, res){
  let tijeloZahtjeva = req.body;
  Korisnik.findAll({where:{'username':tijeloZahtjeva.querry}}).then (function (r) {
    res.send(r);
  })
});






















app.listen(3000);
